
## This Dockerfile creates a Debian with iptables image to run on Docker.
##
## Details available at https://bitbucket.org/arbiza/iptables-docker
## -----------------------------------------------------------------------------


## Use Debian image
FROM debian:jessie

LABEL mantainer="Lucas Arbiza" \
      description="iptables on Docker"


## Kernel Parameters
# Enable IPv6 and IPv4 forwarding
RUN echo 'net.ipv4.ip_forward = 1' >> /etc/sysctl.conf && \
    echo 'net.ipv6.conf.all.forwarding = 1' >> /etc/sysctl.conf && \
    echo 'net.ipv6.conf.default.forwarding = 1' >> /etc/sysctl.conf


## Wrapper script
ADD run.bash /
RUN chmod 755 /run.bash


## Update the system and install required packages
RUN apt-get update && apt-get install -y \
    apt-utils \
    iptables \
    tcpdump \
    vim


## Configuration files
RUN mkdir /etc/iptables && mkdir /etc/bridge
ADD config/iptables /etc/iptables
ADD config/bridge /etc/bridge


VOLUME /etc/iptables /etc/bridge

ENTRYPOINT ["/bin/bash", "/run.bash"]
