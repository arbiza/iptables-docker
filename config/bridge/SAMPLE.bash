#!/bin/bash

BR='br0'
IFs='eth0 eth1 eth2'

IP='/bin/ip'

# Create the bridge and bring it up
$IP link add name $BR type bridge
$IP link set $BR up

# Bring interfaces up and add them to the bridge
for interface in $IFs
do
    $IP link set $interface up
    $IP link set $interface master $BR
done
